# PHP test

## 1. Installation

  - create an empty database named "phptest" on your MySQL server
  - import the dbdump.sql in the "phptest" database
  - put your MySQL server credentials in the constructor of DB class
  - you can test the demo script in your shell: "php index.php"

## 2. Expectations

This simple application works, but with very old-style monolithic codebase, so do anything you want with it, to make it:

  - easier to work with
  - more maintainable


## Changes and Improvements for NewsManager.php:

    Singleton Pattern:
        Improved the singleton pattern by directly instantiating the class within the getInstance method.

    Prepared Statements:
        Used prepared statements in addNews and deleteNews to prevent SQL injection.

    Comments and Documentation:
        Added PHPDoc comments for methods to describe their purpose, parameters, and return types.

    Code Cleanup:
        Improved code readability and consistency by using consistent formatting and indentation.
        Renamed $id in the second loop of deleteNews to $commentId to avoid variable shadowing.

    Return Types:
        Added specific return types for methods to indicate what they are returning (e.g., an array of News objects, an integer ID, or a boolean).

## Changes and Improvements for CommentManager.php:

    Singleton Pattern:
        Simplified the singleton pattern by directly instantiating the class within the getInstance method.

    Prepared Statements:
        Used prepared statements in addCommentForNews and deleteComment to prevent SQL injection.

    Comments and Documentation:
        Added PHPDoc comments for methods to describe their purpose, parameters, and return types.

    Code Cleanup:
        Improved code readability and consistency by using consistent formatting and indentation.
        Renamed variables for clarity ($n to $comment in listComments).

    Return Types:
        Added specific return types for methods to indicate what they are returning (e.g., an array of Comment objects, an integer ID, or a boolean).

## Changes and Improvements for DB.php:

    Singleton Pattern:
        Simplified the singleton pattern by directly instantiating the class within the getInstance method.

    Error Handling:
        Added try-catch block in the constructor to handle potential PDO connection errors.

    PDO Attributes:
        Set PDO attributes for error mode and default fetch mode for better error reporting and easier data handling.

    Prepared Statements:
        Updated select and exec methods to use prepared statements, enhancing security and preventing SQL injection.

    Documentation:
        Added PHPDoc comments for methods to describe their purpose, parameters, and return types.

    Parameter Binding:
        Introduced $params in select and exec methods for parameter binding, improving security and flexibility.

## Changes and Improvements for Comment.php and News.php:

    Visibility Modifiers:
        Explicitly specified the visibility of each property (protected) and method (public).

    PHPDoc Comments:
        Added PHPDoc comments to each method, describing its purpose, parameters, and return types. This helps with code readability and IDE autocompletion.

    Consistent Method Ordering:
        Grouped the set and get methods together for each property to improve code organization and readability.

