<?php

class CommentManager
{
    private static $instance = null;

    private function __construct()
    {
        require_once(ROOT . '/utils/DB.php');
        require_once(ROOT . '/class/Comment.php');
    }

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * List all comments
     * @return array An array of Comment objects
     */
    public function listComments()
    {
        $db = DB::getInstance();
        $rows = $db->select('SELECT * FROM `comment`');

        $comments = [];
        foreach ($rows as $row) {
            $comment = new Comment();
            $comments[] = $comment->setId($row['id'])
                ->setBody($row['body'])
                ->setCreatedAt($row['created_at'])
                ->setNewsId($row['news_id']);
        }

        return $comments;
    }

    /**
     * Add a comment for a specific news item
     * @param string $body The comment body
     * @param int $newsId The ID of the news item
     * @return int The ID of the newly inserted comment
     */
    public function addCommentForNews($body, $newsId)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare('INSERT INTO `comment` (`body`, `created_at`, `news_id`) VALUES (:body, :created_at, :news_id)');
        $stmt->execute([
            ':body' => $body,
            ':created_at' => date('Y-m-d'),
            ':news_id' => $newsId
        ]);
        return $db->lastInsertId();
    }

    /**
     * Delete a comment by its ID
     * @param int $id The ID of the comment to delete
     * @return bool True on success, false on failure
     */
    public function deleteComment($id)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare('DELETE FROM `comment` WHERE `id` = :id');
        return $stmt->execute([':id' => $id]);
    }
}
