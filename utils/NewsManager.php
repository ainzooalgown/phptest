<?php

class NewsManager
{
    private static $instance = null;

    private function __construct()
    {
        require_once(ROOT . '/utils/DB.php');
        require_once(ROOT . '/utils/CommentManager.php');
        require_once(ROOT . '/class/News.php');
    }

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * List all news
     * @return array An array of News objects
     */
    public function listNews()
    {
        $db = DB::getInstance();
        $rows = $db->select('SELECT * FROM `news`');

        $news = [];
        foreach ($rows as $row) {
            $n = new News();
            $news[] = $n->setId($row['id'])
                ->setTitle($row['title'])
                ->setBody($row['body'])
                ->setCreatedAt($row['created_at']);
        }

        return $news;
    }

    /**
     * Add a record in news table
     * @param string $title
     * @param string $body
     * @return int The ID of the newly inserted record
     */
    public function addNews($title, $body)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare('INSERT INTO `news` (`title`, `body`, `created_at`) VALUES (:title, :body, :created_at)');
        $stmt->execute([
            ':title' => $title,
            ':body' => $body,
            ':created_at' => date('Y-m-d')
        ]);
        return $db->lastInsertId();
    }

    /**
     * Delete a news item and its linked comments
     * @param int $id The ID of the news item to delete
     * @return bool True on success, false on failure
     */
    public function deleteNews($id)
    {
        $commentManager = CommentManager::getInstance();
        $comments = $commentManager->listComments();
        $idsToDelete = [];

        foreach ($comments as $comment) {
            if ($comment->getNewsId() == $id) {
                $idsToDelete[] = $comment->getId();
            }
        }

        foreach ($idsToDelete as $commentId) {
            $commentManager->deleteComment($commentId);
        }

        $db = DB::getInstance();
        $stmt = $db->prepare('DELETE FROM `news` WHERE `id` = :id');
        return $stmt->execute([':id' => $id]);
    }
}
