<?php

class DB
{
    private $pdo;

    private static $instance = null;

    private function __construct()
    {
        $dsn = 'mysql:dbname=phptest;host=127.0.0.1';
        $user = 'root';
        $password = '';

        try {
            $this->pdo = new \PDO($dsn, $user, $password);
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            throw new \Exception('Database connection failed: ' . $e->getMessage());
        }
    }

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Executes a SELECT query and returns the results.
     * @param string $sql The SQL query to execute.
     * @param array $params Parameters to bind to the query.
     * @return array The fetched results.
     */
    public function select($sql, $params = [])
    {
        $sth = $this->pdo->prepare($sql);
        $sth->execute($params);
        return $sth->fetchAll();
    }

    /**
     * Executes a non-SELECT query (e.g., INSERT, UPDATE, DELETE).
     * @param string $sql The SQL query to execute.
     * @param array $params Parameters to bind to the query.
     * @return int The number of affected rows.
     */
    public function exec($sql, $params = [])
    {
        $sth = $this->pdo->prepare($sql);
        $sth->execute($params);
        return $sth->rowCount();
    }

    /**
     * Returns the ID of the last inserted row.
     * @return string The last insert ID.
     */
    public function lastInsertId()
    {
        return $this->pdo->lastInsertId();
    }
}
