<?php

define('ROOT', __DIR__);
require_once(ROOT . '/utils/NewsManager.php');
require_once(ROOT . '/utils/CommentManager.php');

// List all news
foreach (NewsManager::getInstance()->listNews() as $news) {
    echo("############ NEWS " . $news->getTitle() . " ############\n");
    echo($news->getBody() . "\n");

    // List comments for each news item
    $comments = CommentManager::getInstance()->listComments();
    foreach ($comments as $comment) {
        if ($comment->getNewsId() == $news->getId()) {
            echo("Comment " . $comment->getId() . " : " . $comment->getBody() . "\n");
        }
    }
}

// Fetch comments separately for other purposes if needed
$commentManager = CommentManager::getInstance();
$comments = $commentManager->listComments();
