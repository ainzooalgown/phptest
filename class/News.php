<?php

class News
{
    protected $id;
    protected $title;
    protected $body;
    protected $createdAt;

    /**
     * Set the ID of the news item.
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get the ID of the news item.
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the title of the news item.
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get the title of the news item.
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the body of the news item.
     * @param string $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * Get the body of the news item.
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set the creation date of the news item.
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Get the creation date of the news item.
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
