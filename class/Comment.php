<?php

class Comment
{
    protected $id;
    protected $body;
    protected $createdAt;
    protected $newsId;

    /**
     * Set the ID of the comment.
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get the ID of the comment.
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the body of the comment.
     * @param string $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * Get the body of the comment.
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set the creation date of the comment.
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Get the creation date of the comment.
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set the ID of the associated news item.
     * @param int $newsId
     * @return $this
     */
    public function setNewsId($newsId)
    {
        $this->newsId = $newsId;
        return $this;
    }

    /**
     * Get the ID of the associated news item.
     * @return int
     */
    public function getNewsId()
    {
        return $this->newsId;
    }
}
